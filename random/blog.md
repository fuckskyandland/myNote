# Web
* MDN--good https://developer.mozilla.org/zh-CN/
* 阮一峰 https://www.ruanyifeng.com/  
* 左耳听风 https://coolshell.cn/

## other
* 谷歌历年题 https://zibada.guru/gcj/

## js教程
* https://wangdoc.com/javascript/  

## typescript
https://wangdoc.com/typescript/intro  

## github  
* 最好用的中文速查表  
https://github.com/skywind3000/awesome-cheatsheets  

* 印记中文  
https://docschina.org/