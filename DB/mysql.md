## 创建Mysql数据库
1. 解压MySQL安装包：tar -zxvf mysql-xxx.tar.gz
2. 进入解压后的目录：cd mysql-xxx/
3. 创建MySQL的数据目录：mkdir data
4. 初始化数据库：./bin/mysqld --user=mysql --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --initialize
5. 启动MySQL服务：./bin/mysqld_safe --user=mysql &
6. 查看进程：ps -ef | grep mysql

# ERROR
issue:  
2023-10-13T13:40:58.289650Z mysqld_safe error: log-error set to '/var/log/mariadb/mariadb.log', however file don't exists. Create writable for user 'mysql'.

solve:  
sudo mkdir -p /var/log/mariadb/  
sudo chown mysql:mysql /var/log/mariadb/   
sudo chmod 755 /var/log/mariadb/  
sudo service mysql restart 或者 sudo systemctl restart mysql

issues:
start failed  

solve:  
service mysql restart
systemctl status mysqld.service

### 创建用户
> create user 'readonly'@'localhost' IDENTIFIED BY 'readonly';
### 创建只读用户
> grant select on jiandan.* TO 'readonly'@'localhost';
> flush privileges;
### 查看用户权限
> use mysql;
> select * from user\G;
### 创建授予用户远程访问权限
> GRANT ALL ON jiandan.* TO 'root'@'192.168.1.150' IDENTIFIED BY 'root';
>
> 