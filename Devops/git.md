# pro git
https://git-scm.com/book/zh/v2

# git 命令
https://gitee.com/all-about-git

# 各种gitignore
https://github.com/github/gitignore

# 一些命令
* git diff --staged  
* git log --stat  提交的简略统计信息  
* git rm --cached add.txt 从暂存区移除文件  
* git commit --amend 修改提交  
* git remote show origin 查看远端信息  


https://git-scm.com/book/zh/v2/Git-%E5%88%86%E6%94%AF-%E8%BF%9C%E7%A8%8B%E5%88%86%E6%94%AF