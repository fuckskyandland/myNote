## docker command
* sudo systemctl start docker
* docker ps -a
* docker exec -it 7ef69493b770 /bin/bash


## 查看容器进程  
* 查看容器进程  
docker top my_mysql
* 查看容器信息
docker inspect name/id  | grep Mounts -A 20

## docker容器
* docker images
* docker pull xxx
* docker search xxx

## docker安装mysql
* docker run -d --name my_mysql -e MYSQL_ROOT_PASSWORD=root  -p 3307:3306 mysql
* SHOW VARIABLES LIKE 'datadir';
默认在/var/lib/mysql/
可以
```
mkdir -p docker_v/mysql/conf
cd docker_v/mysql/conf
touch my.cnf
docker run -p 3306:3306 --name mysql -v /opt/docker_v/mysql/conf:/etc/mysql/conf.d -e MYSQL_ROOT_PASSWORD=123456 -d imageID
```

## blog  
https://blog.csdn.net/m0_58523831/article/details/131693934
https://blog.csdn.net/qq_52497256/article/details/128870731

## Harbor私有仓库的安装
https://blog.csdn.net/weixin_41465338/article/details/80146218?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-1-80146218-blog-103769241.235%5Ev38%5Epc_relevant_sort&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-1-80146218-blog-103769241.235%5Ev38%5Epc_relevant_sort&utm_relevant_index=2
