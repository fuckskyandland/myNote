## 盒子
盒子宽度，没有设置width的是时候，默认是auto,是以父节点的宽度，完全填充的；
设置padding、marging、border的宽度都会缩短内容的宽度，整体还是全部填充父节点；
当设置了宽度后，设置padding、marging、border的宽度都会使设置的宽度增加；
设置box-sizing属性后，即使已经设置了width还是会和auto时产生一样的结果。

## 浮动--float and clear
浮动非图片元素时，必须给它设定宽度，否则后果难以预料。图片无所谓，因为它本身有默认的宽度。
使用float之后会使元素脱离文档流，对父元素不可见，后续的元素继续按照文档流排列。
解决float后排列问题：
1. 给容器元素应用overflow:hidden
2. 同时浮动父元素，父元素设置width和float
3. 给父元素的最后添加一个非浮动的子元素，然后清除该子元素;或者给父元素添加一个伪元素
   .clearfix:after {
        content:".";
        display:block;
        height:0;
        visibility:hidden;
        clear:both;
    }

## 定位--position
static--默认
relative--相对之前static的位置的定位，设置top、left等设置位置
absolute--相对于body的定位
fixed--相对于窗口

## 显示--display
block--块级元素,占一行，啥都都可以设置
inline--行内元素，a、span和img，在一行中margin left和right没有效果
inline-block--有block和inline的优点
flex
grid

##
